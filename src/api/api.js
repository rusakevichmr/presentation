import axios from "axios";
import { KEY } from "./api_key";
const instance = axios.create({
  withCredentials: true,
  baseURL: `https://social-network.samuraijs.com/api/1.0/`,
  headers: {
    "API-KEY": `${KEY}`,
  },
});

const userAPI = {
  getUsers(currentPage, pageSize = 5) {
    return instance
      .get(`users?page=${currentPage}&count=${pageSize}`)
      .then((response) => response.data);
  },

  unFollow(id) {
    return instance.delete(`follow/${id}`).then((response) => response.data);
  },

  follow(id) {
    return instance.post(`follow/${id}`).then((response) => response.data);
  },

  getProfile(userID) {
    return instance.get(`/profile/${userID}`).then((response) => response.data);
  },

  auth() {
    return instance.get(`/auth/me`).then((response) => response.data);
  },

  login(data) {
    return instance
      .post(`auth/login`, { ...data })
      .then((response) => response.data);
  },

  logout() {
    return instance.delete(`auth/login`).then((response) => response.data);
  },
};

const postAPI = {};

const profileAPI = {
  getProfile(userID) {
    return instance.get(`/profile/${userID}`).then((response) => response.data);
  },
  getStatus(userID) {
    return instance
      .get(`/profile/status/${userID}`)
      .then((response) => response.data);
  },
  updateStatus(statusValue) {
    return instance
      .put("/profile/status", { status: statusValue })
      .then((response) => response.data);
  },
};

export { userAPI, postAPI, profileAPI };

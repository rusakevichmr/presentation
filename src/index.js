import React from "react";
import ReactDOM from "react-dom";
import "./index.scss";
import store from "./Redux/redux_store";
import App from "./components/App";
import { BrowserRouter } from "react-router-dom";
import { Provider } from "react-redux";
import "./App.scss";
ReactDOM.render(
  <BrowserRouter>
    <Provider store={store}>
      <App store={store} />
    </Provider>
  </BrowserRouter>,

  document.getElementById("root")
);

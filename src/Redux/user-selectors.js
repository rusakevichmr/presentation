export const getUsersListS = (state) => {
  return state.userReducer.user;
};

export const getPageSizeS = (state) => {
  return state.userReducer.pageSize;
};

export const getTotalUserCountS = (state) => {
  return state.userReducer.totalUserCount;
};

export const getCurrentPageS = (state) => {
  return state.userReducer.currentPage;
};

export const getIsFetchingS = (state) => {
  return state.userReducer.isFetching;
};

export const getFollowingProgressS = (state) => {
  return state.userReducer.followingProgress;
};

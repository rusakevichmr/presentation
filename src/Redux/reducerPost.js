const ADD_POST = `ADD-POST`;
const UPDATE_NEW_POST_TEXT = `UPDATE-NEW-POST-TEXT`;
const FOLLOW = `FOLLOW`;
const UN_FOLLOW = `UN-FOLLOW`;
const SET_POST = `SET-POST`;
const SET_NEW_POST = `SET-POST`;
const TOGGLED_IF_FETCHING = `TOOTLED-IF-FETCHING`;
const TOGGLED_FOLLOWING = `TOGGLED-FOLLOWING`;

let initialState = {
  post: [
    {
      name: `Maks`,
      id: `1`,
      messages: `Hello`,
      followed: false,
      avatar: `https://www.meme-arsenal.com/memes/8abad17ae081384956a7084acfb2f8e4.jpg`,
    },
    {
      name: `Vasa`,
      id: `2`,
      messages: `Hello`,
      followed: false,
      avatar: `https://www.meme-arsenal.com/memes/8abad17ae081384956a7084acfb2f8e4.jpg`,
    },
  ],
  newPost: `Новое сообщение`,
};

const messagesReducer = (state = initialState, action) => {
  let stateCopy = { ...state, post: [...state.post] };

  switch (action.type) {
    case SET_POST:
      stateCopy.post.push(action.post);
      return stateCopy;

    case SET_NEW_POST:
      return stateCopy;

    case FOLLOW:
      stateCopy.post.map((e) => {
        if (e.id === action.id) {
          e.followed = true;
        }
        return e;
      });

      return stateCopy;

    case UN_FOLLOW:
      stateCopy.post.map((e) => {
        if (e.id === action.id) {
          e.followed = false;
        }
        return e;
      });
      return stateCopy;
    default:
      return state;
  }
};

export const setPostActionCreator = (post) => {
  return {
    type: SET_POST,
    post: post,
  };
};

export const setNewPostActionCreator = (post) => {
  return {
    type: SET_NEW_POST,
    post: post,
  };
};

export const followActionCreator = (id) => {
  return {
    type: FOLLOW,
    id: id,
  };
};

export const unFollowActionCreator = (id) => {
  return {
    type: UN_FOLLOW,
    id: id,
  };
};
export const toggledIsFetchingActionCreator = (isFetching) => {
  return {
    type: TOGGLED_IF_FETCHING,
    isFetching: isFetching,
  };
};

export const toggledFollowProgressActionCreator = (followingProgress, id) => {
  return {
    type: TOGGLED_FOLLOWING,
    followingProgress: followingProgress,
    id: id,
  };
};

export const addMessagesActionCreator = () => {
  return {
    type: ADD_POST,
  };
};

export const updateNewMessagesTextActionCreator = (newText) => {
  return {
    type: UPDATE_NEW_POST_TEXT,
    text: newText,
  };
};

export const postFollowUnFollowThunkCreator = (id, flag) => {
  return (dispatch) => {
    if (flag) {
      dispatch(followActionCreator(id));
    } else {
      dispatch(unFollowActionCreator(id));
    }
  };
};

export const getPostThunkCreator = () => {
  return (dispatch) => {
    // dispatch(setPostActionCreator());
  };
};

export default messagesReducer;

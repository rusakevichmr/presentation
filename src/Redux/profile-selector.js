import { createSelector } from "reselect";

export const profileS = (state) => {
  return state.profileReducer.profile;
};

export const profileHeavySelector = createSelector(profileS, (profile) => {
  return profile;
});

import { userAPI } from "./../api/api.js";
import { stopSubmit } from "redux-form";
const SET_USER_DATA = `AUTH-SET-USER-DATA`;
const TOGGLED_FOLLOWING = `AUTH-TOGGLED-FOLLOWING`;

let initialState = {
  id: null,
  login: null,
  email: null,
  auth: false,
  followingProgress: false,
};

const authReducer = (state = initialState, action) => {
  switch (action.type) {
    case TOGGLED_FOLLOWING:
      return { ...state, followingProgress: action.followingProgress };

    case SET_USER_DATA:
      return { ...state, ...action.data };
    default:
      return state;
  }
};

export const setUserDataAC = (id, login, email, auth) => {
  return {
    type: SET_USER_DATA,
    data: { id, login, email, auth },
  };
};

export const toggledFollowProgressAC = (followingProgress) => {
  return {
    type: TOGGLED_FOLLOWING,
    followingProgress: followingProgress,
  };
};

export const getUserThunkCreator = () => {
  return async (dispatch) => {
    try {
      let data = await userAPI.auth();
      let { id, login, email, auth = true } = data.data;
      if (data.resultCode === 0) {
        dispatch(setUserDataAC(id, login, email, auth));
      } else {
        dispatch(setUserDataAC(null, null, null, null));
        if (data.resultCode === 1) {
          console.error(`not authorized`);
        } else {
          console.error(`${data.resultCode} getUser`);
        }
      }
    } catch {
      console.error(`Not auth`);
    }
  };
};

export const authUserThunkCreator = (dataUser) => {
  return async (dispatch) => {
    dispatch(toggledFollowProgressAC(true));
    try {
      let data = await userAPI.login(dataUser);
      if (data.resultCode === 0) {
        dispatch(getUserThunkCreator());
      } else {
        let message =
          data.messages.length > 0 ? data.messages[0] : `Some error`;
        dispatch(stopSubmit(`login`, { _error: message }));
      }
      dispatch(toggledFollowProgressAC(false));
    } catch (err) {
      console.error(`Not response authUser`);
      dispatch(toggledFollowProgressAC(false));
    }
  };
};

export const outUserThunkCreator = (dataUser) => {
  return async (dispatch) => {
    dispatch(toggledFollowProgressAC(true));
    try {
      let data = await userAPI.logout();
      if (data.resultCode === 1) {
        console.error(` outUser not success`);
      }
      dispatch(toggledFollowProgressAC(false));
      dispatch(setUserDataAC(null, null, null, false));
    } catch (err) {
      console.error(`Not response outUser`);
    }
  };
};

export default authReducer;

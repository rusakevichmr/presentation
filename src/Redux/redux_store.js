import userReducer from "./reducerUser";
import postReducer from "./reducerPost";
import profileReducer from "./reducerProfile";
import appReducer from "./reducerApp";

import { combineReducers, createStore, applyMiddleware, compose } from "redux";
import authReducer from "./reducerAuth";
import thunkMiddleware from "redux-thunk";
import { reducer as formReducer } from "redux-form";

let reducers = combineReducers({
  userReducer: userReducer,
  postReducer: postReducer,
  profileReducer: profileReducer,
  authReducer: authReducer,
  appReducer: appReducer,
  form: formReducer,
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(
  reducers,
  composeEnhancers(applyMiddleware(thunkMiddleware))
);

// let store = createStore(reducers, applyMiddleware(thunkMiddleware));
window.store = store;
export default store;

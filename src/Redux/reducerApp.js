import { getUserThunkCreator } from "./reducerAuth";
const INITIALIZE_SUCCESS = `APP-INITIALIZE`;

let initialState = {
  initialize: false,
};

const appReducer = (state = initialState, action) => {
  switch (action.type) {
    case INITIALIZE_SUCCESS:
      return { ...state, initialize: true };
    default:
      return state;
  }
};

export const setInitializeSuccess = () => {
  return {
    type: INITIALIZE_SUCCESS,
  };
};

export const initializeAppTC = (dataUser) => {
  return async (dispatch) => {
    let promise = dispatch(getUserThunkCreator());

    try {
     await Promise.all([promise]);
      dispatch(setInitializeSuccess());
    } catch {
      console.error(`initializeApp failed`);
    }
  };
};

export default appReducer;

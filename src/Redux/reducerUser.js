import { userAPI } from "./../api/api.js";

const ADD_NEW_USER = `USER-ADD-NEW-USER`;
const UPDATE_NEW_USER_TEXT = `USER-UPDATE-NEW-USER-TEXT`;
const SET_USER = `USER-SET-USER`;
const UN_FOLLOW = `USER-UN-FOLLOW`;
const FOLLOW = `USER-FOLLOW`;
const SET_CURRENT_PAGE = `USER-SET-CURRENT-PAGE`;
const SET_TOTAL_USER_COUNT = `USER-SET-TOTAL-USER-COUNT`;
const TOGGLED_IF_FETCHING = `USER-TOOTLED-IF-FETCHING`;
const TOGGLED_FOLLOWING = `USER-TOGGLED-FOLLOWING`;

let initialState = {
  user: [
    {
      follow: ``,
      name: "",
      photos: { small: null, large: null },
      followingProgress: false,
    },
  ],
  pageSize: 5,
  totalUserCount: 0,
  currentPage: 1,
  isFetching: true,
};

const userReducer = (state = initialState, action) => {
  let stateCopy = { ...state, user: [...state.user] };
  switch (action.type) {
    case TOGGLED_FOLLOWING:
      stateCopy.user.map((e) => {
        if (e.id === action.id) {
          e.followingProgress = action.followingProgress;
        }
        return e;
      });
      return stateCopy;

    case TOGGLED_IF_FETCHING:
      return { ...state, isFetching: action.isFetching };

    case SET_TOTAL_USER_COUNT:
      return { ...state, totalUserCount: action.userCount };

    case SET_CURRENT_PAGE:
      return { ...state, currentPage: action.page };

    case SET_USER:
      return { ...state, user: action.user };

    case FOLLOW:
      stateCopy.user.map((e) => {
        if (e.id === action.id) {
          e.followed = true;
        }
        return e;
      });

      return stateCopy;

    case UN_FOLLOW:
      stateCopy.user.map((e) => {
        if (e.id === action.id) {
          e.followed = false;
        }
        return e;
      });
      return stateCopy;

    default:
      return state;
  }
};

export const toggledIsFetchingActionCreator = (isFetching) => {
  return {
    type: TOGGLED_IF_FETCHING,
    isFetching: isFetching,
  };
};

export const setPageActionCreator = (page) => {
  return {
    type: SET_CURRENT_PAGE,
    page: page,
  };
};

export const setUserActionCreator = (user) => {
  return {
    type: SET_USER,
    user: user,
  };
};

export const addNewUserActionCreator = (newUserName) => {
  return {
    type: ADD_NEW_USER,
    userName: newUserName,
  };
};
export const updateNewUserTextActionCreator = (newUser) => {
  return {
    type: UPDATE_NEW_USER_TEXT,
    newUser: newUser,
  };
};

export const followActionCreator = (id) => {
  return {
    type: FOLLOW,
    id: id,
  };
};

export const unFollowActionCreator = (id) => {
  return {
    type: UN_FOLLOW,
    id: id,
  };
};

export const setTotalUserCount = (userCount) => {
  return {
    type: SET_TOTAL_USER_COUNT,
    userCount: userCount,
  };
};
export const toggledFollowProgressActionCreator = (followingProgress, id) => {
  return {
    type: TOGGLED_FOLLOWING,
    followingProgress: followingProgress,
    id: id,
  };
};

export const getUserThunkCreator = (pageNumber, pageSize) => {
  return (dispatch) => {
    dispatch(setPageActionCreator(pageNumber));
    dispatch(toggledIsFetchingActionCreator(true));
    userAPI.getUsers(pageNumber, pageSize).then((data) => {
      dispatch(toggledIsFetchingActionCreator(false));
      dispatch(setUserActionCreator(data.items));
      dispatch(setTotalUserCount(data.totalCount));
    });
  };
};

export const followThunkCreator = (id, flag) => {
  return async (dispatch) => {
    if (flag === true) {
      dispatch(toggledFollowProgressActionCreator(true, id));
      try {
        let data = await userAPI.follow(id);
        dispatch(toggledFollowProgressActionCreator(false, id));
        if (data.resultCode === 0) {
          dispatch(followActionCreator(id));
        } else {
          console.error(`follow ${data.resultCode}`);
        }
      } catch {
        console.log(`not response follow`);
        dispatch(toggledFollowProgressActionCreator(false, id));
      }
    } else {
      dispatch(toggledFollowProgressActionCreator(true, id));
      try {
        dispatch(toggledFollowProgressActionCreator(false, id));
        let data = await userAPI.unFollow(id);
        if (data.resultCode === 0) {
          dispatch(unFollowActionCreator(id));
        } else {
          console.error(`unFollow ${data.resultCode}`);
        }
      } catch {
        dispatch(toggledFollowProgressActionCreator(false, id));
        console.error(`unFollow error`);
      }
    }
  };
};

export default userReducer;

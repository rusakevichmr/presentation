import { profileAPI } from "./../api/api.js";

const SET_USER_PROFILE = `PROFILE-SET-USER-PROFILE`;
const SET_STATUS = `PROFILE-SET-STATUS`;
const UPDATE_STATUS = `PROFILE-UPDATE_STATUS`;
let initialState = {
  profile: null,
  status: ``,
};

const messagesReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_USER_PROFILE:
      return { ...state, profile: action.profile };
    case SET_STATUS:
      return { ...state, status: action.status };
    case UPDATE_STATUS:
      return { ...state, status: action.status };

    default:
      return state;
  }
};

export const setUserProfileAC = (profile) => {
  return {
    type: SET_USER_PROFILE,
    profile: profile,
  };
};

export const setStatusAC = (status) => {
  return {
    type: SET_STATUS,
    status: status,
  };
};

export const updateStatusAC = (status) => {
  return {
    type: UPDATE_STATUS,
    status: status,
  };
};

// export const getUserThunkCreator = (userID) => {
//   return (dispatch) => {
//     if (userID === `null`) {
//       dispatch(setUserProfileAC(null));
//     } else {
//       profileAPI.getProfile(userID).then((data) => {
//         dispatch(setUserProfileAC(data));
//       });
//     }
//   };
// };

export const getUserThunkCreator = (userID) => {
  return async (dispatch) => {
    if (userID === `null`) {
      dispatch(setUserProfileAC(null));
    } else {
      try {
        let data = await profileAPI.getProfile(userID);
        dispatch(setUserProfileAC(data));
      } catch {
        dispatch(setUserProfileAC(null));
        // throw new Error(`Not response getUser`);
        console.error(`Not response getUserProfile`);
      }
    }
  };
};

export const getStatusThunkCreator = (userID) => {
  return async (dispatch) => {
    try {
      let data = await profileAPI.getStatus(userID);
      if (data.resultCode !== 0) {
        dispatch(setStatusAC(data));
      } else {
        console.error(`getStatus ${data.resultCode}`);
      }
    } catch {
      console.error(`not get data status user`);
      dispatch(setStatusAC(`not get data status user`));
    }
  };
};

export const updateStatusThunkCreator = (status) => {
  console.log(`bbb`);
  return async (dispatch) => {
    try {
      await profileAPI.updateStatus(status);
      dispatch(setStatusAC(status));
    } catch {
      console.error(`status user not update`);
    }
  };
};

export const authUser = (data) => {};

export default messagesReducer;

import postReducer, { setPostActionCreator } from "./reducerPost";

let action = setPostActionCreator({
  name: `Maks`,
  id: `1`,
  messages: `Hello`,
  followed: false,
  avatar: `https://www.meme-arsenal.com/memes/8abad17ae081384956a7084acfb2f8e4.jpg`,
});

let state = {
  post: [
    {
      name: `Maks`,
      id: `1`,
      messages: `Hello`,
      followed: false,
      avatar: `https://www.meme-arsenal.com/memes/8abad17ae081384956a7084acfb2f8e4.jpg`,
    },
    {
      name: `Vasa`,
      id: `2`,
      messages: `Hello`,
      followed: false,
      avatar: `https://www.meme-arsenal.com/memes/8abad17ae081384956a7084acfb2f8e4.jpg`,
    },
  ],
};

it("new Messages to should be added", () => {
  let newState = postReducer(state, action);
  expect(newState.post.length).toBe(3);
  expect(newState.post[2]).toHaveProperty(`id`);
  expect(newState.post[2]).toHaveProperty(`messages`, `Hello`);
});

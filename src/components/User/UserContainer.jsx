import React from "react";
import User from "./User";
import Preloader from "../common/Preloader/Preloader";
import {
  getUserThunkCreator,
  followThunkCreator,
} from "../../Redux/reducerUser";
import { connect } from "react-redux";
import { withAuthRedirect } from "../../hoc/withAuthRedirect";
import {
  getUsersListS,
  getPageSizeS,
  getTotalUserCountS,
  getCurrentPageS,
  getIsFetchingS,
  getFollowingProgressS,
} from "./../../Redux/user-selectors";
import { compose } from "redux";
class UserContainer extends React.Component {
  componentDidMount() {
    this.props.getUserThunkCreator(1, this.props.pageSize);
  }

  opPageChanged = (pageNumber) => {
    this.props.getUserThunkCreator(pageNumber, this.props.pageSize);
  };

  unFollow = (id) => {
    this.props.followThunkCreator(id, false);
  };

  follow = (id) => {
    this.props.followThunkCreator(id, true);
  };

  render() {
    return (
      <>
        {this.props.isFetching ? <Preloader /> : null}
        <User
          opPageChanged={this.opPageChanged}
          userList={this.props.userList}
          totalUserCount={this.props.totalUserCount}
          pageSize={this.props.pageSize}
          currentPage={this.props.currentPage}
          unFollow={this.unFollow}
          follow={this.follow}
        />
      </>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    userList: getUsersListS(state),
    pageSize: getPageSizeS(state),
    totalUserCount: getTotalUserCountS(state),
    currentPage: getCurrentPageS(state),
    isFetching: getIsFetchingS(state),
    followingProgress: getFollowingProgressS(state),
  };
};

export default compose(
  connect(mapStateToProps, {
    getUserThunkCreator,
    followThunkCreator,
  }),
  withAuthRedirect
)(UserContainer);

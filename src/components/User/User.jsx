import React from "react";
import s from "./User.module.scss";
import userPhoto from "../../assets/img/userPhoto.jpg";
import { NavLink } from "react-router-dom";

let User = (props) => {
  let pagesCount = Math.ceil(props.totalUserCount / props.pageSize);
  let pages = [];
  for (let i = 1; i <= pagesCount; i++) {
    if (i === props.currentPage + 5) {
      break;
    } else {
      if (i <= props.currentPage - 5) {
        continue;
      } else {
        pages.push(i);
      }
    }
  }

  return (
    <div>
      {props.userList.map((user) => {
        return (
          <div key={user.name} className={s.dialog}>
            <NavLink to={`Profile/` + user.id}>
              <img
                className={s.Avatar}
                alt={`oops`}
                src={user.photos.small != null ? user.photos.small : userPhoto}
              />
            </NavLink>
            <div>{user.name}</div>
            <div>
              {user.followed ? (
                <button
                  disabled={user.followingProgress}
                  className={s.buttonPost}
                  onClick={() => {
                    props.unFollow(user.id);
                  }}
                >
                  Unfollowed
                </button>
              ) : (
                <button
                  disabled={user.followingProgress}
                  className={s.buttonPost}
                  onClick={() => {
                    props.follow(user.id);
                  }}
                >
                  Follow
                </button>
              )}
            </div>
          </div>
        );
      })}
      <div className={s.select}>
        {pages.map((e) => {
          return (
            <span
              className={props.currentPage === e ? s.red : ``}
              key={e + `button`}
              onClick={() => {
                props.opPageChanged(e);
              }}
            >
              {e}
            </span>
          );
        })}
      </div>
    </div>
  );
};

export default User;

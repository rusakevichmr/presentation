import React from "react";
import HeaderContainer from "./header/HeaderContainer";
import UserContainer from "./User/UserContainer";
import { Route, withRouter } from "react-router-dom";
import PostContainer from "./Post/PostContainer";
import ProfileContainer from "./Profile/ProfileContainer";
import LoginContainer from "./Login/LoginContainer";
import { Component } from "react";
import { connect } from "react-redux";
import { initializeAppTC } from "./../Redux/reducerApp";
import { compose } from "redux";
import Preloader from "./common/Preloader/Preloader";

class App extends Component {
  componentDidMount() {
    this.props.initializeAppTC();
  }

  render() {
    if (this.props.initialize) {
      return (
        <div>
          <HeaderContainer />
          <Route path="/Login" render={() => <LoginContainer />} />
          <Route path="/User" render={() => <UserContainer />} />
          <Route path="/Post" render={() => <PostContainer />} />
          <Route path="/Profile/:userId?" render={() => <ProfileContainer />} />
        </div>
      );
    } else {
      return <Preloader />;
    }
  }
}

let mapStateToProps = (state) => ({
  initialize: state.appReducer.initialize,
});
export default compose(
  withRouter,
  connect(mapStateToProps, {
    initializeAppTC,
  })
)(App);

// export default App;

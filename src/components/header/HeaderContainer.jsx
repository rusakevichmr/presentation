import React from "react";
import "../../App.scss";
import Header from "./Header";
import { connect } from "react-redux";
import { compose } from "redux";

class HeaderContainer extends React.Component {


  render() {
    return (
      <>
        <Header
          id={this.props.id}
          login={this.props.login}
          loginAction={this.loginAction}
        />
      </>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    id: state.authReducer.id,
    login: state.authReducer.login,
  };
};
export default compose(
  connect(mapStateToProps, {}),
  
)(HeaderContainer);

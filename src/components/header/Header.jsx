import React from "react";
import "../../App.scss";
import s from "./head.module.scss";
import Clock from "./Clock/Clock";
import { NavLink } from "react-router-dom";

function header(props) {
  return (
    <header className={s.head}>
      {
        <div>
          {props.login ? (
            <div>
              <NavLink to={"/Login/" + props.id}>{props.login}</NavLink>
            </div>
          ) : (
            <div>
              <NavLink to={"/Login/" + props.id}>UserLogin</NavLink>
            </div>
          )}
        </div>
      }

      <div>
        <div>
          <NavLink to="/there_is_nothing">there is nothing</NavLink>
        </div>
        <div>
          <NavLink to="/Post">Post</NavLink>
        </div>
        <div>
          <NavLink to="/User">User</NavLink>
        </div>
        <div>
          <NavLink to={`/Profile/` + props.id}>Profile</NavLink>
        </div>
        <Clock />
      </div>
    </header>
  );
}

export default header;

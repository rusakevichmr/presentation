import React from "react";
import s from "./Login.module.scss";
import { Field, reduxForm } from "redux-form";
import { required } from "../../utils/validator/validator";
import { Input } from "../common/form/FormsControls";
import { asyncValidate } from "./../../utils/validator/asyncValidator";

const LoginForm = (props) => {
  console.log(props);
  return (
    <form className={s.form} onSubmit={props.handleSubmit}>
      <div className={s.field}>
        <Field
          name="email"
          placeholder={"email"}
          component={Input}
          type="email "
          validate={[required]}
        />
      </div>
      <div className={s.field}>
        <Field
          placeholder={"Password"}
          name="Password"
          type={"Password"}
          component={Input}
          validate={[required]}
        />
      </div>
      <div className={s.field}>
        <Field component="input" name="rememberMe" type={"checkbox"} />
        <label htmlFor="firstName">Remember Me</label>
      </div>
      {props.error && <div className={s.form_summary_error}>{props.error}</div>}

      <div className={s.field}>
        <button>Login</button>
      </div>
    </form>
  );
};

const LoginReduxForm = reduxForm({
  form: `login`,
  asyncValidate,
})(LoginForm);

const Login = (props) => {
  return (
    <div className={s.center}>
      <div className={s.login}>
        {props.auth ? (
          <div className={s.form}>
            <div>{props.login}</div>
            <button
              className={s.buttonLogin}
              onClick={props.offSubmit}
              disabled={props.followingProgress}
            >
              LogOut
            </button>
          </div>
        ) : (
          <div>
            <LoginReduxForm onSubmit={props.onSubmit} />
          </div>
        )}
      </div>
    </div>
  );
};

export default Login;

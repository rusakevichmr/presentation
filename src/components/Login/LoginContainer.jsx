import React from "react";
import Login from "./Login";
// import Axios from "axios";
import { connect } from "react-redux";
import {
  setUserDataAC,
  authUserThunkCreator,
  outUserThunkCreator,
} from "../../Redux/reducerAuth";
import { compose } from "redux";
import { withRouter } from "react-router-dom";

class LoginContainer extends React.Component {
  onSubmit = (data) => {
    if (!this.props.followingProgress) {
      this.props.authUserThunkCreator(data);
      console.log(`thank`);
    }
  };
  offSubmit = () => {
    this.props.outUserThunkCreator();
  };
  render() {
    return (
      <div>
        <Login
          {...this.props}
          onSubmit={this.onSubmit}
          offSubmit={this.offSubmit}
        />
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  auth: state.authReducer.auth,
  email: state.authReducer.email,
  id: state.authReducer.id,
  login: state.authReducer.login,
  followingProgress: state.authReducer.followingProgress,
});

export default compose(
  connect(mapStateToProps, {
    setUserDataAC,
    authUserThunkCreator,
    outUserThunkCreator,
  }),
  withRouter
)(LoginContainer);

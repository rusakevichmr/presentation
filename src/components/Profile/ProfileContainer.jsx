import React from "react";
import Profile from "./Profile";
import { connect } from "react-redux";
import {
  getUserThunkCreator,
  getStatusThunkCreator,
  updateStatusThunkCreator,
} from "../../Redux/reducerProfile";
import { withRouter } from "react-router-dom";
import { withAuthRedirect } from "../../hoc/withAuthRedirect";
import { compose } from "redux";
import { profileHeavySelector } from "./../../Redux/profile-selector";
class ProfileContainer extends React.Component {
  getUser() {
    let userID = this.props.match.params.userId;
    if (!userID) {
      userID = this.props.profile.userId;
      if (!userID) {
        this.props.history.push("/User");
      }
    }
    this.props.getUserThunkCreator(userID);
    this.props.getStatusThunkCreator(userID);
  }

  componentDidMount() {
    this.getUser();
  }
  componentDidUpdate() {
    // this.getUser();
  }

  render() {
    return (
      <>
        <Profile {...this.props} />
      </>
    );
  }
}
let mapStateToProps = (state) => ({
  profile: profileHeavySelector(state),
  status: state.profileReducer.status,
});

export default compose(
  connect(mapStateToProps, {
    getUserThunkCreator,
    getStatusThunkCreator,
    updateStatusThunkCreator,
  }),
  withRouter,
  withAuthRedirect
)(ProfileContainer);

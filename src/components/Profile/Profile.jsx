import React from "react";
import ProfileInfo from "./ProfileInfo/ProfileInfo";

const Profile = (props) => {
  return (
    <div>
      <ProfileInfo
        {...props.profile}
        status={props.status}
        getStatus={props.getStatusThunkCreator}
        updateStatus={props.updateStatusThunkCreator}
      />
    </div>
  );
};

export default Profile;

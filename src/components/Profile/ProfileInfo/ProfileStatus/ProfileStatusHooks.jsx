import React, { useState, useEffect } from "react";
import s from "./../ProfileInfo.module.scss";
// import { useEffect } from "react";
const ProfileStatusHooks = (props) => {
  let [editMode, setEditMode] = useState(false);
  let [status, setStatus] = useState(props.status);

  useEffect(() => {
    setStatus(props.status);
  }, [props.status]);

  const activateMod = () => {
    setEditMode(true);
  };
  const deactivate = () => {
    setEditMode(false);
    props.updateStatus(status);
  };

  const onStatusChange = (e) => {
    setStatus(e.currentTarget.value);
  };
  return (
    <div>
      {!editMode ? (
        <div>
          <span className={s.status} onDoubleClick={activateMod}>
            {props.status}
          </span>
        </div>
      ) : (
        <div>
          <input
            onChange={onStatusChange}
            onDoubleClick={deactivate}
            onBlur={deactivate}
            autoFocus={true}
            value={status}
          />
        </div>
      )}
    </div>
  );
};

export default ProfileStatusHooks;

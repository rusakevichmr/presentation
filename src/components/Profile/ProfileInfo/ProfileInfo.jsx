import React from "react";
import link from "./ProfileInfo.module.scss";
import Preloader from "../../common/Preloader/Preloader";
import userPhoto from "../../../assets/img/userPhoto.jpg";
import ProfileStatusHooks from "./ProfileStatus/ProfileStatusHooks";

const ProfileInfo = (props) => {
  if (!props.photos) {
    return <Preloader />;
  }

  let cont = props.contacts;
  let contKey = Object.keys(cont);
  let contactRender = contKey.map((e) => {
    return (
      <li key={e}>
        <span className={link.span15}>{e}:</span>
        {!cont[e] ? `none` : cont[e]};
      </li>
    );
  });
  return (
    <div>
      <div>
        <ProfileStatusHooks
          status={props.status}
          getStatus={props.getStatus}
          updateStatus={props.updateStatus}
        />
      </div>

      <div>
        {props.photos.large ? (
          <img src={props.photos.large} alt="oops" />
        ) : (
          <img className="AvatarLarge" src={userPhoto} alt="oops" />
        )}
      </div>
      <div>{props.aboutMe}</div>
      <ul>{contactRender}</ul>
      <div>
        <span className={link.span30}>lookingForAJob</span>{" "}
        {props.lookingForAJob ? `да` : `нет`}
      </div>
      <div>
        <span className={link.span30}>lookingForAJobDescription</span>{" "}
        {props.lookingForAJobDescription}
      </div>
      <div>
        <span className={link.span30}>fullName</span> {props.fullName}
      </div>
      <div>
        <span className={link.span30}>userId</span> {props.userId}
      </div>
    </div>
  );
};

export default ProfileInfo;

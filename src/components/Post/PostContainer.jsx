import React from "react";
import Post from "./Post";
import { connect } from "react-redux";
import {
  getPostThunkCreator,
  postFollowUnFollowThunkCreator,
  setNewPostActionCreator,
} from "../../Redux/reducerPost";
import { withAuthRedirect } from "../../hoc/withAuthRedirect";
import { compose } from "redux";

class PostContainer extends React.Component {
  componentDidMount() {
    this.props.getPostThunkCreator();
  }

  followed = (id) => {
    this.props.postFollowUnFollowThunkCreator(id, true);
  };

  unFollowed = (id) => {
    this.props.postFollowUnFollowThunkCreator(id, false);
  };

  addMessage = (data) => {
    console.log({});
    this.props.setNewPostActionCreator({
      messages: data.messages,
      id: this.props.id,
      name: this.props.login,
      followed: false,
      avatar: `https://www.meme-arsenal.com/memes/8abad17ae081384956a7084acfb2f8e4.jpg`,
    });
  };

  render() {
    return (
      <>
        <Post
          post={this.props.post}
          newPost={this.props.newPost}
          followed={this.followed}
          unFollowed={this.unFollowed}
          addMessage={this.addMessage}
        />
      </>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    post: state.postReducer.post,
    newPost: state.postReducer.newPost,
    id: state.authReducer.id,
    login: state.authReducer.login,
  };
};

export default compose(
  connect(mapStateToProps, {
    postFollowUnFollowThunkCreator,
    getPostThunkCreator,
    setNewPostActionCreator,
  }),
  withAuthRedirect
)(PostContainer);

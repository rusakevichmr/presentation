import React from "react";
import s from "./post.module.scss";
import { Field, reduxForm } from "redux-form";
import { required, maxLengthCreator } from "./../../utils/validator/validator";
import { Textarea } from "./../common/form/FormsControls";
const maxLength10 = maxLengthCreator(10);

const NewMessage = (props) => {
  // console.log(`render`);

  return (
    <form onSubmit={props.handleSubmit}>
      <div>
     
        <Field
          name="messages"
          placeholder={"New message"}
          component={Textarea}
          validate={[required, maxLength10]}
        />
      </div>
      <div>
        <button>New message</button>
      </div>
    </form>
  );
};

const NewMessageForm = reduxForm({
  form: `NewMessage`,
})(NewMessage);

const Post = (props) => {
  let userAndButton = (user) => (
    <div>
      {user.followed ? (
        <button
          className={s.buttonPost}
          onClick={() => {
            props.unFollowed(user.id);
          }}
        >
          Unfollowed
        </button>
      ) : (
        <button
          className={s.buttonPost}
          onClick={() => {
            props.followed(user.id);
          }}
        >
          Follow
        </button>
      )}
    </div>
  );

  return (
    <div>
      {props.post.map((user, index) => (
        <div key={index} className={s.flex}>
          <img className={s.Avatar} src={user.avatar} alt="" />
          <div>{userAndButton(user)}</div>
          <div className={s.line}>
            <div> {user.name} </div>
            <div> {user.messages}</div>
          </div>
        </div>
      ))}
      <NewMessageForm onSubmit={props.addMessage} />
    </div>
  );
};

export default Post;

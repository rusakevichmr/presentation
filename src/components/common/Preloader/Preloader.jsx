import React from "react";
import s from "./Preloader.module.scss";
import preloader from "./../../../assets/gif/preloader.gif";

let Preloader = () => {
  return (
    <div className={s.center}>
      <img className={s.border} src={preloader} alt="#" />
    </div>
  );
};

export default Preloader;
